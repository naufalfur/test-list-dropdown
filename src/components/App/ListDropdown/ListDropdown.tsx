// interface ListDropdownProps {
//
// }

import { useDebounce } from "@uidotdev/usehooks";
import {useEffect, useState} from "react";

function ListDropdown() {
    const [value, setValue] = useState<string>('')
    const [debouncedValue, setDebouncedValue] = useState<string>('');
    const [products, setProducts] = useState<any[]>([]); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10

    const products_api = "https://dummyjson.com/products";

    const fetchProducts = async (inputValue:string) => {
        try {
            const response = await fetch(products_api);
            const data = await response.json();

            const products = data.products

            const filteredProducts = products.filter((product: any) => {
                return product.title.toLowerCase().includes(inputValue.toLowerCase()) || product.id.toString().includes(inputValue);
            })
            // ascending order by title
                .sort((a: any, b: any) => {
                    return a.title.localeCompare(b.title);
                })


            setProducts(filteredProducts)
        } catch {
            console.log("Error fetching products")
        }
    }

    const handleChange = (e: any) => {
        setValue(e.target.value);
    }

    useEffect(() => {
        fetchProducts(value)
    }, [value])


    return (
        <div>
            {/*input search*/}
            <input type="text"
                   value={value}
                   onChange={handleChange}
            />
            {/*Output the filteredProducts*/}
            <ul>
                {products.map((product: any) => {
                    return <li key={product.id}>{product.title}</li>
                })}
            </ul>

        </div>
    )
}

export default ListDropdown